import "./App.css";
import Footer from "./components/footer/footer";
import Header from "./components/header/Header";
import Projects from "./components/projects/Projects";

function App() {
  return (
    <>
      <Header />
      <Projects />
      <Footer />
    </>
  );
}

export default App;
