import { RoughNotation } from "react-rough-notation";
import projects from "../../assets/projects.json";
import Section from "./Section";

function Projects() {
  return (
    <>
      <ul className="listProjectsContainer">
        <li>
          <RoughNotation
            type="underline"
            show={true}
            animationDelay={1500}
            animate={true}
            color="yellow"
            iterations={10}
            strokeWidth={0.4}
            padding={[-1, 0, -25, -10]}
          >
            <h2 className="titleProjects">Projects</h2>
          </RoughNotation>
        </li>
        {projects.projects.map((project: any, index: number) => {
          return (
            <li key={project.name + index}>
              <Section
                name={project.name}
                description={project.description}
                link={project.link}
                img={project.img}
              />
            </li>
          );
        })}
      </ul>
    </>
  );
}

export default Projects;
