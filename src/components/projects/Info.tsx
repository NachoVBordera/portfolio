function Info({ name, description }: any) {
  return (
    <>
      <section className="infoContainer">
        <h2>{name}</h2>
        <p>{description} </p>
      </section>
    </>
  );
}

export default Info;
