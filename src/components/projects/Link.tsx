import chinch from "../../assets/Thumb_Tack_clip_art_medium.png";

function Link({ img, link }: any) {
  return (
    <>
      <section className="linkContainer">
        <p className="rotateP">GO!</p>
        <span className="ArrowSpan"></span>
        <a
          className="link"
          target="_blank"
          rel="noopener noreferrer"
          href={link}
        >
          <img className="imgLink" src={img} alt="" />
          <img className="chinch" src={chinch} alt="" />
        </a>
      </section>
    </>
  );
}

export default Link;
