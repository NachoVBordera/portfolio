import Info from "./Info";
import Link from "./Link";

function Section({ name, description, link, img }: any) {
  return (
    <section className="projectsContainer">
      <span className="fixProject"></span>
      <span className="fix2"></span>
      <section className="contentContainer">
        <Info name={name} description={description} />
        <Link link={link} img={img} />
      </section>
    </section>
  );
}

export default Section;
