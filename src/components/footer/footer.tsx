function Footer() {
  return (
    <section className="FooterContainer">
      <p>Made with ❤️ Nacho</p>
    </section>
  );
}

export default Footer;
