import { RoughNotation } from "react-rough-notation";

function About() {
  return (
    <section className="postIt">
      <p className="postItP">
        I am passionate about the {"   "}
        <RoughNotation
          show={true}
          type={"circle"}
          animationDelay={1100}
          padding={[7, 3, 8, 3]}
          color="#252525"
        >
          mental challenges
        </RoughNotation>{" "}
        Constantly seeking to learn and acquire new tools to tackle them
        effectively.
        <br />
        <br />I enjoy collaborating in{"  "}
        <RoughNotation
          show={true}
          type={"underline"}
          animationDelay={1500}
          padding={[4, 4, 0, 4]}
        >
          diverse teams
        </RoughNotation>{" "}
        , as I firmly believe that the variety of perspectives enriches the
        creative process. Embracing different{" "}
        <RoughNotation
          show={true}
          type="highlight"
          color="yellow"
          animationDelay={2000}
          padding={[4, 4, 0, 4]}
        >
          viewpoints
        </RoughNotation>{" "}
        from my peers allows personal and professional growth.
      </p>
    </section>
  );
}

export default About;
