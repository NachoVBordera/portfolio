import { RoughNotation } from "react-rough-notation";
import linkedIn from "../../assets/linkedin.png";
import git from "../../assets/git.png";
import cv from "../../assets/cv.png";

function Links() {
  return (
    <>
      <h1 style={{ fontSize: 60 }}>NACHO V. BORDERA</h1>
      <section className="linksListContainer">
        <RoughNotation
          show={true}
          type="box"
          iterations={5}
          animationDelay={1000}
          color="black"
          padding={[10, 10, 10, 10]}
        >
          <ul className="linksList">
            <li>
              <a
                href="https://www.linkedin.com/in/nacho-v-bordera-a386b1261/"
                target="_blank"
                rel="noopener noreferrer"
              >
                <img src={linkedIn} alt="LinkedIn" className="imageLink" />
              </a>
            </li>
            <li>
              {" "}
              <a
                href="https://firebasestorage.googleapis.com/v0/b/portfolionacho-39e42.appspot.com/o/curriculum-ignacio-V2.pdf?alt=media&token=2022cb30-30e8-4a43-ae52-5f4f49c97b0b"
                download="Curriculum.pdf"
              >
                <img src={cv} alt="GitLab" className="imageLink" />
              </a>
            </li>
            <li>
              {" "}
              <a
                href="https://github.com/NachoVBordera"
                target="_blank"
                rel="noopener noreferrer"
              >
                <img src={git} alt="GitLab" className="imageLink" />
              </a>
            </li>
          </ul>
        </RoughNotation>
      </section>
    </>
  );
}

export default Links;
