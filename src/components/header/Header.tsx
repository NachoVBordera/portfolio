import About from "./About";
import Links from "./Links";
import Photo from "./Photo";

function Header() {
  return (
    <header>
      <section className="photoLinkSection">
        <Photo />
        <Links />
      </section>
      <About />
    </header>
  );
}

export default Header;
