import nacho from "../../assets/Group.png";
import stickerSmall from "../../assets/evolution.png";
import stickerAstro from "../../assets/1330058003.png";

function Photo() {
  return (
    <section className="PhotoSestion">
      <span id="imageMain">
        <img id="photo" src={nacho} alt="Personal image" />
        <img id="small" src={stickerSmall} alt="Personal image" />
        <img id="astro" src={stickerAstro} alt="Personal image" />
      </span>
    </section>
  );
}

export default Photo;
